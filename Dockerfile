FROM elixir:alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY config config
COPY mix.exs .
COPY mix.lock .

RUN MIX_ENV=prod mix local.hex --force && \
    mix local.rebar --force && \
    mix deps.get && \
    mix deps.compile

COPY lib lib
RUN MIX_ENV=prod mix release

ENTRYPOINT ["_build/prod/rel/pshendwich/bin/pshendwich", "start"]
