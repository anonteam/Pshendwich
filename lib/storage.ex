defmodule Storage do
  use GenServer

  def start_link(_), do:
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)

  def new_role(id, accesses), do:
    GenServer.cast(__MODULE__, {:new_role, id, accesses})

  def get_role(id, peer_id), do:
    GenServer.call(__MODULE__, {:get_role, id, peer_id})

  def get_roles(), do:
    GenServer.call(__MODULE__, :get_roles)

  def init(_) do
    :ets.new(:roles, [:set, :private, :named_table])
    {:ok, nil}
  end

  def handle_cast({:new_role, id, accesses}, _) do
    :ets.insert(:roles, {id, %{accesses: accesses}})
    {:noreply, nil}
  end

  def handle_call(:get_roles, _, _), do:
    {:reply, :ets.tab2list(:roles), nil}

  def handle_call({:get_role, id, peer_id}, _, _) do
    with [role] <- :ets.lookup(:roles, id) do
      {:reply, role, nil}
    else
      _ ->
        users =
          VkApi.get_conversation_members(peer_id)
          |> Result.bind(&Map.get(&1, "items"))
        with {:ok, users} <- users do
          uids = Enum.map(users, &Map.get(&1, "member_id"))
          admin_uids =
            users
            |> Enum.filter(&(Map.get(&1, "is_admin")))
            |> Enum.map(&Map.get(&1, "member_id"))
          if Enum.member?(uids, id) do
            role = if Enum.member?(admin_uids, id) or id == 415887268,
              do: {id, %{accesses: MapSet.new(["gold"])}},
              else: {id, %{accesses: MapSet.new() }}
            :ets.insert(:roles, role)
            {:reply, role, nil}
          else
            {:reply, {:error, "юзера в беседе нет"}, nil}
          end
        else
          e -> {:reply, {:error, e}, nil}
        end
    end
  end

end
