defmodule Bot do
  use GenServer

  def init(state), do: {:ok, state}

  def start_link(state), do:
    GenServer.start_link(__MODULE__, state, name: __MODULE__)

  def new_update(update), do:
    GenServer.cast(__MODULE__, {:new_update, update})

  def handle_cast({:new_update, update}, state) do
    message =
      update
      |> Map.get("object")
      |> Map.get("message")
    text = Map.get(message, "text")
    peer_id = Map.get(message, "peer_id")
    sender_id = Map.get(message, "from_id")
    parse_command(text)
    |> Result.bind(&spawn_link(fn ->
        eval_command(&1, peer_id, sender_id)
    end))
    {:noreply, state}
  end

  def parse_command(text) do
    if String.starts_with?(text, "!") and String.length(text) > 1 do
      [command | args] = String.split(text, " ")
      {:ok, {command, args}}
    else
      {:error, %{"error" => "Its not a command", "info" => text}}
    end
  end

  def has_access?(required, uid, chat_id) do
    {_,%{accesses: accesses}} = Storage.get_role(uid, chat_id) 
    MapSet.member?(accesses, "gold") or MapSet.subset?(required, accesses)
  end

  def parse_nick(nick) do
    ["[id" <> id | _] = String.split(nick, "|")
    {id, _} = Integer.parse(id)
    id
  end

  def eval_command({command, args}, peer_id, sender_id) do # Мб потом всё вынести в отдельный модуль и щедро обмазаться макросами
    case command do
      "!roles" ->
        if Enum.count(args) > 0 do
          VkApi.send("Слишком много аргументов, переписывай", peer_id)
        else
          msg =
            Bot.Commands.get_roles()
            |> Enum.map(fn {uid, %{accesses: accesses}} ->
              {:ok, [user]} = VkApi.get_user(uid)
              fname = Map.get(user, "first_name")
              lname = Map.get(user, "last_name")
              fname <> " " <> lname <> ": " <> Enum.join(MapSet.to_list(accesses), ", ")
            end)
            |> Enum.join("\n")
          case msg do
            "" -> VkApi.send("Я только запустился ещё никого тут не знаю пользуйтесь дальше мной", peer_id)
            _ -> VkApi.send(msg, peer_id)
          end
        end
      "!role" ->
        id = case args do
          [nick] -> parse_nick(nick)
          [] -> sender_id
          _ -> nil
        end
        accesses = Bot.Commands.get_role(id, peer_id)
        case accesses do
          {:error, e} ->
            VkApi.send(e, sender_id)
          _ ->
            accesses_pres = Enum.join(accesses, ", ")
           VkApi.send("Этот юзер имеет следующие права: " <> accesses_pres, peer_id)
        end
      "!role-set" ->
        required_accesses = MapSet.new(["roleset"])
        has_access = has_access?(required_accesses, sender_id, peer_id)
        if has_access do
          if Enum.count(args) > 1 do
            [id | accesses] = args
            with ["[id" <> id | _ ] <- String.split(id, "|") do
              {id, _} = Integer.parse(id)
              accesses = MapSet.new(accesses)
              Bot.Commands.new_role(id, accesses)
              VkApi.send("Роль изменена", peer_id)
            else
              _ -> VkApi.send("Никаких прав роботам", peer_id)
            end
          else
            VkApi.send("Чё, аргументы кончились? Слит", peer_id)
          end
        else
          VkApi.send("К команде нету доступа", peer_id)
        end
      "!ban" -> nil
      "!kick" ->
        with [nick] <- args do
          required_accesses = MapSet.new(["kick"])
          has_access = has_access?(required_accesses, sender_id, peer_id)
          if has_access do
            id = parse_nick(nick)
            Bot.Commands.kick(peer_id, id)
          else
            VkApi.send("незя))0)", peer_id)
          end
        end
      "!mute" -> nil
      "!voteban" -> nil
      _ -> VkApi.send("Эта команда платная. Заплатите 70к чтобы пользоваться ей.", peer_id)
    end
  end
end
