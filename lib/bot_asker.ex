defmodule Bot.Asker do
  use GenServer
  def init(_) do
    Process.send_after(__MODULE__, :ping, 5000)
    {:ok, nil}
  end

  def start_link(_), do:
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)

  def handle_info(_, state) do
    VkApi.get_events()
    |> Map.get("updates")
    |> Enum.each(&Bot.new_update/1)
    Process.send_after(__MODULE__, :ping, 500)
    {:noreply, state}
  end
end
