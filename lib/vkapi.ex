defmodule VkApi do
  use GenServer

  def start_link(state), do:
    GenServer.start_link(__MODULE__, state, name: __MODULE__)

  def init(state), do:
    state
    |> Result.wrap()
    |> Result.bind(&VkApi.get_lp_server/1)
    |> Result.bind(&Map.merge(state, &1))

  def send(message, chat_id, args \\ %{}), do:
    GenServer.cast(__MODULE__, {:send, message, chat_id, args})

  def get_events(), do:
    GenServer.call(__MODULE__, :get_events)

  def get_user(username), do:
    GenServer.call(__MODULE__, {:get_user, username})

  def get_conversation_members(peer_id), do:
    GenServer.call(__MODULE__, {:get_conversation_members, peer_id})

  def remove_chat_user(peer_id, uid), do:
    GenServer.call(__MODULE__, {:remove_chat_user, peer_id, uid})

  def invoke(method, args, state) do
    method_url =
      "https://api.vk.com/method/" <> method
    token = Map.get(state, "access_token")
    ver = Map.get(state, "v")

    args
    |> Map.merge(%{"access_token" => token, "v" => ver})
    |> URI.encode_query()
    |> then(&HTTPoison.get(method_url <> "?" <> &1))
    |> Result.bind(&Map.get(&1, :body))
    |> Result.bind(&Jason.decode/1)
    |> Result.bind(&VkApi.check_errors/1)
    |> Result.bind(&Map.get(&1, "response"))
  end

  def check_errors(response) do
    err = Map.get(response, "error")
    if err != nil do # Здарова гошники
      {:error, err}
    else
      {:ok, response}
    end
  end

  def get_lp_server(state) do
    id = Map.get(state, "group_id")
    VkApi.invoke("groups.getLongPollServer", %{"group_id" => id}, state)
  end

  def handle_call(:get_events, _, state) do
    server = Map.get(state, "server")
    response =
      %{"act" => "a_check",
        "key" => Map.get(state, "key"),
        "ts"  => Map.get(state, "ts")}
      |> URI.encode_query()
      |> then(&HTTPoison.get(server <> "?" <> &1))
      |> Result.bind(&Map.get(&1, :body))
      |> Result.bind(&Jason.decode/1)
    case response do
      {:ok, %{"failed" => code}} -> {:stop, {:error, "ошибка с кодом" <> Integer.to_string(code)}, nil}
      {:ok, events} -> {:reply, events, %{state | "ts" => Map.get(events, "ts")}}
      {:error, error} -> {:stop, error, nil}
    end
  end

  def handle_call({:get_user, username}, _, state) do
    user = VkApi.invoke("users.get", %{user_ids: username}, state)
    {:reply, user, state}
  end

  def handle_call({:get_conversation_members, peer_id}, _, state) do
    result = VkApi.invoke("messages.getConversationMembers", %{"peer_id" => peer_id}, state)
    {:reply, result, state}
  end

  def handle_call({:remove_chat_user, peer_id, uid}, _, state) do
    result = VkApi.invoke("messages.removeChatUser", %{"chat_id" => peer_id - 2000000000, "user_id" => uid}, state)
    {:reply, result, state}
  end

  def handle_cast({:send, message, chat_id, args}, state) do
    response =
      %{"random_id" => :rand.uniform(),
        "peer_id"   => chat_id,
        "message"   => message}
      |> Map.merge(args)
      |> then(&VkApi.invoke("messages.send", &1, state))
    case response do
      {:ok, _} -> {:noreply, state}
      {:error, err} -> {:stop, err, nil}
    end
  end



end

defmodule Result do

  def bind({:ok, value}, fun) do
    case fun.(value) do
      {:ok, val} -> {:ok, val}
      {:error, mes} -> {:error, mes}
      :error -> :error
      val -> {:ok, val}
    end
  end

  def bind({:error, message}, _), do:
    {:error, message}

  def wrap(val), do:
    {:ok, val}

end
