defmodule Pshendwich.Application do
  use Application

  def start(_type, _args) do
    api_config = %{
      "access_token" => Application.get_env(:pshendwich, :token),
      "v" => Application.get_env(:pshendwich, :v),
      "group_id" => Application.get_env(:pshendwich, :group_id),
    }

    children = [
      VkApi.child_spec(api_config),
      Storage.child_spec(nil),
      Bot.child_spec(nil),
      Bot.Asker.child_spec(nil)
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end

end
