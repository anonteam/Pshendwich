defmodule Bot.Commands do
  def get_roles() do
    Storage.get_roles()
    |> Enum.filter(fn {_, %{accesses: accesses}} ->
      (MapSet.size(accesses) != 0) end)
  end
  def new_role(uid, accesses), do:
    Storage.new_role(uid, accesses)

  def get_role(uid, peer_id) do
    {_,%{accesses: accesses}} = Storage.get_role(uid, peer_id)
    accesses
  end
  def kick(peer_id, uid), do:
    VkApi.remove_chat_user(peer_id, uid)

end
